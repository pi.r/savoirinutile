#!/usr/bin/python3.7
import asyncio
import configparser
import os
import pickle
import sys
from dataclasses import dataclass
from pathlib import Path

from markdown import markdown
from nio import AsyncClient


@dataclass
class Savoir:
    text: str
    source: str


async def main():
    current_path = Path(os.path.realpath(__file__)).parent
    cfg_file = current_path / "savoirinutile.cfg"
    if not cfg_file.exists():
        print(f"config file {cfg_file} not found")
        sys.exit(1)
    config = configparser.ConfigParser()
    config.read(cfg_file)
    host = config.get("user", "host")
    username = config.get("user", "username")
    token = config.get("user", "token")
    room = config.get("user", "room")
    db_path = current_path / "savoirinutile.pkl"
    with open(db_path, "rb") as f:
        savoirs = pickle.load(f)
    index_file = current_path / "index.txt"
    with open(index_file, "r") as f:
        index = int(f.read())
    savoir = savoirs[index]
    message = f"Le saviez vous ? {savoir.text}"
    if savoir.source:
        message += f" [[source]({savoir.source})]"
    content = {
        "body": message,
        "formatted_body": markdown(message),
        "msgtype": "m.text",
        "format": "org.matrix.custom.html",
    }
    client = AsyncClient(host, username)
    client.access_token = token
    await client.room_send(room, "m.room.message", content)
    with open(index_file, "w") as f:
        f.write(str(index + 1))


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
